console.log("Hello World");

let firstName = "John";
let lastName = "Smith";
let age = "30";

console.log("First Name: " + firstName);
console.log("Last Name: " + lastName);
console.log("Age: " + age);
console.log("Hobbies:");

let hobbies = ["Biking", "Mountain Climbing", "Swimming"];

console.log(hobbies);

console.log("Work Address:")
let workAddress = {
	houseNumber: "32",
	street: "Washington",
	city: "Lincoln",
	state: "Nebraska"
};
console.log(workAddress);

console.log(firstName + ' ' + lastName + ' ' + "is " + age + " years of age.");

console.log("This was printed inside of the function");

function printHobby(hobbies){
	console.log(hobbies);
}
printHobby(hobbies);

console.log("This was printed inside of the function");

function printAddress(workAddress){
	console.log(workAddress);
}
printAddress(workAddress);

let isMarried = true;

console.log("The value of isMarried is: " + isMarried)


